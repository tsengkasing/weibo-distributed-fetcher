
/**
 * 普通日志
 * @param {Array<string>} msg
 */
exports.logInfo = function(...msg) {
    console.log(`[${new Date().toLocaleString({}, {
        timeZone: 'Asia/Shanghai'
    })}][INFO] ${msg.join('\t')}`);
}

/**
 * 报错日志
 * @param {Array<string>} msg
 */
exports.logError = function(...msg) {
    console.error(`[${new Date().toLocaleString({}, {
        timeZone: 'Asia/Shanghai'
    })}][ERROR] ${msg.join('\t')}`);
}

/**
 * 等待
 * @param {number} milliseconds 
 */
exports.waitFor = function(milliseconds) {
    return new Promise(resolve => {
    setTimeout(() => resolve(), milliseconds);
  });
}

