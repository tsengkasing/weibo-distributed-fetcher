const fs = require('fs');
const path = require('path');
const request = require('request-promise-native');

const { logInfo, logError, waitFor } = require('./logger');

class WeiboFetcher {
  constructor() {
    this._jar = request.jar();
    this._fetch = request.defaults({
      jar: this._jar,
      simple: false,
      headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; ServiceUI 14) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763' },
    });
  }

  /**
   * 重置 cookie
   */
  reset() {
    this._jar = request.jar();
    this._fetch = request.defaults({
      jar: this._jar,
      simple: false,
      headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; ServiceUI 14) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763' },
    });
  }

  get jar() {
    return this._jar;
  }

  /**
   * 爬取微博
   * @param {string} uid 用户 uid
   * @param {number} minYear 最小抓取年份
   * @param {async function} callback function every page
   * @returns {Array<{itemid: string, scheme: string, mblog: {created_at: string, text: string, source: string, textLength: number}}>}
   */
  async fetchWeibos(uid, minYear = 2018, callback = async () => {}) {
    const fetch = this._fetch;
    let json, jsonText;

    const weibos = [];
    let keepFetching = true;
    let pageNum = '';
    while (keepFetching) {
      await waitFor(Math.max(Math.random() * 6, 1) * 5000);
      try {
        jsonText = await fetch.get(
          `https://m.weibo.cn/api/container/getIndex?page=${pageNum}&type=uid&value=${uid}&containerid=107603${uid}`
        );
        fs.writeFileSync(path.join(__dirname, '..', 'temp', `weibo-${uid}${pageNum ? `-${pageNum}` : ''}.json`), jsonText, { flag: 'w' });  
        json = JSON.parse(jsonText);
      } catch (err) {
        logError('[weibo-fetcher]', '可能爬虫已经被封', err);
        continue;
      }
      let {ok, data} = json;
      if (!ok) break;
      logInfo(`Fetching Page ${pageNum} ${ok ? 'OK' : 'Fail'}!`);

      /**
       * @type {{cardlistInfo: {page: number}, cards: Array<{itemid: string, scheme: string, mblog: {created_at: string, text: string, source: string, textLength: number}}>}}
       */
      const { cardlistInfo: { page }, cards } = data;
      pageNum = parseInt(page, 10);

      if (!Array.isArray(cards)) continue;
      await callback(cards);
      weibos.push(...cards);

      // minYear 之前的不获取
      if (cards.some(({mblog: {created_at}}) => created_at.length === 10 && (new Date(created_at)).getFullYear() < minYear)) {
        keepFetching = false;
      }
    }
    return weibos;
  }
}

module.exports = WeiboFetcher;
