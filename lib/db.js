const mysql = require('mysql2/promise');

class DB {
    /**
     * constructor
     * @param {string} host
     * @param {string} user
     * @param {string} password
     * @param {string} database
     */
    constructor(host, user, password, database) {
        this._config = {
            host,
            user,
            password,
            database
        };

        this._timer = null;
        this._connection = null;
    }

    get isConnected() {
        return !!this._connection;
    }

    async connect() {
        if (this._timer) clearTimeout(this._timer);
        this._connection = await mysql.createConnection(this._config);
        const ping = connection => {
            connection.ping(err => {
                if (err) console.error(err);
                else console.log(`[${(new Date()).toLocaleString()}] Database Connection OK`);
            });
            this._timer = setTimeout(ping, 60 * 60 * 1000, connection);
        };
        ping(this._connection);
    }

    close() {
        if (this._timer) clearTimeout(this._timer);
        this._connection.end();
    }

    /**
     * Fetch one row
     * @param {string} query SQL
     * @param {Array<string | number>} param parameters
     */
    async fetchOne(query, param) {
        if (!this._connection) throw new Error('Please Connect Database.');
        const [rows] = await this._connection.query(query, param);
        return rows[0];
    }

    async fetchAll(query, param) {
        if (!this._connection) throw new Error('Please Connect Database.');
        const [rows] = await this._connection.query(query, param);
        return rows;
    }

    async execute(query, param) {
        if (!this._connection) throw new Error('Please Connect Database.');
        try {
            await this._connection.execute(query, param);
            return {ok: true};
        } catch (err) {
            return {ok: false, msg: err};
        }
    }
}

module.exports = DB;
