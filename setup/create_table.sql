DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `uid` CHAR(20) UNIQUE NOT NULL,
  `scanned` TINYINT(1) DEFAULT 0,
  `location` CHAR(30) NULL,
  `timestamp` INT(11),
  KEY (`uid`)
);

DROP TABLE IF EXISTS `weibo`;
CREATE TABLE `weibo` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `uid` CHAR(20) NOT NULL,
  `item_id` VARCHAR(50) UNIQUE NOT NULL,
  `scheme` VARCHAR(200) NULL,
  `create_date` CHAR(16) NOT NULL,
  `text` NVARCHAR(4000) NOT NULL,
  `source` NVARCHAR(20) NULL,
  `text_length` INT(10) NULL,
  KEY (`uid`),
  KEY (`create_date`),
  KEY (`item_id`)
) CHARACTER SET utf8mb4;

ALTER TABLE `weibo` CONVERT TO CHARACTER SET utf8mb4;

DROP TABLE IF EXISTS `worker_statistic`;
CREATE TABLE `worker_statistic` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `worker` CHAR(20) NOT NULL,
  `weibo_num` INT NULL,
  `timestamp` INT(11),
  KEY (`timestamp`)
);
