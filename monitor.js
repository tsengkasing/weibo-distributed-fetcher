/**
 * @fileoverview
 * 监控
 */

const fs = require('fs');
const path = require('path');
const http = require('http');
const url = require('url');

const { logError, logInfo } = require('./lib/logger');
const DB = require('./lib/db');

const {
  HTTP_PORT,
  DB: {HOST, USER, PASSWORD, DATABASE},
} = require('./config.json');

const workers = new Set();
// const fetchingStatus = new Map();

(async function main() {
  const db = await setUpDatabase();
  /* let uids = await db.fetchAll('SELECT `uid` FROM `user` WHERE NOT `scanned` = 2 AND `location` LIKE "%北京%"');
  uids.forEach(uid => {
    fetchingStatus.set(uid, false);
  });
  uids.map(({uid}) => uid); */

  setUpHttpServer(db);
})();

/**
 * SQL Database
 * @returns {Promise<DB>}
 */
async function setUpDatabase() {
  // MySQL
  const db = new DB(HOST, USER, PASSWORD, DATABASE);
  await db.connect();
  return db;
}

/**
 * @param {DB} db
 */
function setUpHttpServer(db) {
  http.createServer(function (request, response) {
    const {pathname: route, searchParams} = new url.URL(request.url, `http://${request.headers.host}`);
    logInfo(route, request.socket.remoteAddress);
    switch (route) {
    case '/':
    case '/index':
    case '/index.html':
      fs.createReadStream(path.join(__dirname, 'public', 'index.html')).pipe(response);
      break;
    case '/favicon.ico':
      fs.createReadStream(path.join(__dirname, 'public', 'favicon.ico')).pipe(response);
      break;
    case '/api/db/connect':
      db.connect().then(() => {
        response.write('OK');
      }).catch(err => {
        response.write(err);
        logError('Database Reconnect', err);
      }).finally(() => {
        response.end();
      });
      break;
    case '/api/worker/status':
      response.setHeader('Content-Type', 'application/json; charset=UTF-8;');
      response.write(JSON.stringify(Array.from(workers)));
      response.end();
      break;
    case '/api/worker/name':
      db.fetchAll('SELECT DISTINCT `worker` AS `name` FROM `worker_statistic`;')
        .then(rows => {
          response.setHeader('Content-Type', 'application/json; charset=UTF-8;');
          response.write(JSON.stringify(rows.map(({name}) => name)));
        }).catch(err => logError('[Query]', err)).finally(() => {
          response.end();
        });
      break;
    case '/api/worker/statistic':
      let num = 30;
      if (searchParams.has('num')) {
        num = parseInt(searchParams.get('num'), 10);
        if (isNaN(num)) { num = 30; }
      }
      loadStatistic(db, num).then(data => {
        response.setHeader('Content-Type', 'application/json; charset=UTF-8;');
        response.write(JSON.stringify(data));
      }).catch(err => logError('[Query]', err)).finally(() => {
        response.end();
      });
      break;
    case '/api/weibo/total':
      db.fetchOne('SELECT COUNT(*) AS `num` FROM `weibo`;')
        .then(row => {
          response.setHeader('Content-Type', 'application/json; charset=UTF-8;');
          response.write(JSON.stringify(row));
        }).catch(err => logError('[Query]', err)).finally(() => {
          response.end();
        });
      break;
    case '/api/weibo/size':
      db.fetchOne("SELECT SUM(DATA_LENGTH) AS `size` FROM `information_schema`.`tables` WHERE `table_schema` = 'weibo' and `table_name` = 'weibo';")
        .then(row => {
          response.setHeader('Content-Type', 'application/json; charset=UTF-8;');
          response.write(JSON.stringify(row));
        }).catch(err => logError('[Query]', err)).finally(() => {
          response.end();
        });
      break;
    default:
      response.statusCode = 404;
      response.write('<script type="application/javascript">window.location.href = "/";</script>');
      response.end();
    }
  }).listen(HTTP_PORT);
}

/**
 * 获取 timeline
 * @param {DB} db
 * @param {number} num 时间点个数
 */
async function loadStatistic(db, num = 30) {
  // 获取所有前 num 个时间点
  const periods = (await db.fetchAll(`
    SELECT
      DISTINCT TIMESTAMPADD(MINUTE, TIMESTAMPDIFF(MINUTE, '2019-01-01', FROM_UNIXTIME(timestamp)), '2019-01-01') AS period
    FROM worker_statistic
    ORDER BY period DESC
    LIMIT ${num};
  `)).map(({period}) => period);
  const workers = (await db.fetchAll('SELECT DISTINCT `worker` AS `name` FROM `worker_statistic`;')).map(({name}) => name);

  return (await Promise.all(
    periods.map(
      async period => ({
        time: (new Date(period)).getTime(),
        data: (await db.fetchAll(`
          SELECT
            worker AS name,
            SUM(weibo_num) AS num,
            TIMESTAMPADD(MINUTE, TIMESTAMPDIFF(MINUTE, '2019-01-01', FROM_UNIXTIME(timestamp)), '2019-01-01') AS time_period
          FROM worker_statistic
          WHERE TIMESTAMP('${period}') = TIMESTAMPADD(MINUTE, TIMESTAMPDIFF(MINUTE, '2019-01-01', FROM_UNIXTIME(timestamp)), '2019-01-01')
          GROUP BY time_period, name;
        `)).sort(({name: A}, {name: B}) => A > B ? 1 : -1)
      })
    )
  )).map(({data, ...props}) => {
    if (data.length === workers.length) return {data, ...props};
    for (let worker of workers) {
      if (data.every(({name}) => name !== worker)) {
        data.push({name: worker, num: 0});
      }
    }
    data.sort(({name: A}, {name: B}) => A > B ? 1 : -1);
    return {data, ...props};
  });
}
