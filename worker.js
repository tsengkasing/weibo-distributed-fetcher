/**
 * @fileoverview
 * 爬虫的 worker
 * 每个节点开启一个 worker
 */

const Redis = require('ioredis');
const Redlock = require('redlock');
const { logInfo, logError, waitFor } = require('./lib/logger');
const DB = require('./lib/db');
const WeiboFetcher = require('./lib/weibo-fetcher');

const {
  WORKER_NAME,
  MIN_YEAR,
  DB: {HOST, USER, PASSWORD, DATABASE},
  REDIS: {HOST: REDIS_HOST, PORT: REDIS_PORT, PASSWORD: REDIS_PASSWORD}
} = require('./config.json');

async function main() {
  // 连接关系型数据库
  const db = new DB(HOST, USER, PASSWORD, DATABASE);
  await db.connect();

  // 连接 Redis
  const redis = new Redis({
    host: REDIS_HOST,
    port: REDIS_PORT,
    password: REDIS_PASSWORD,
  });
  const redlock = new Redlock([redis], { retryCount: 3 });
  redlock.on('clientError', function (err) {
    logError('[redis]', err);
  });

  let uids = await db.fetchAll('SELECT `uid` FROM `user` WHERE NOT `scanned` = 2 AND `num_weibo` > 10;');
  uids = uids.map(({uid}) => uid);
  const weiboFetcher = new WeiboFetcher();
  while (true) {
    const uid = uids[parseInt(Math.random() * (uids.length - 1), 10)];
    let lock;
    try {
      lock = await redlock.lock(uid, 6000000);
    } catch (err) {
      logError('[redlock]', err);
      continue;
    }

    // 抓取并存入数据库
    logInfo(`Fetching User ${uid}`);
    await weiboFetcher.fetchWeibos(uid, MIN_YEAR, async (weibos) => {
      const sql = weibos.map(({itemid, scheme, mblog: { created_at, text, source, textLength }}) => {
        const now = new Date();
        if (/([0-9]*)分钟前/.test(created_at)) {
          let [, minutes] = /([0-9]*)分钟前/.exec(created_at);
          now.setMinutes(now.getMinutes() - parseInt(minutes, 10));
          created_at = formatTime(now);
        } else if (/([0-9]*)小时前/.test(created_at)) {
          let [, hours] = /([0-9]*)小时前/.exec(created_at);
          now.setHours(now.getHours() - parseInt(hours, 10));
          created_at = formatTime(now);
        } else if (created_at.length < 6) {
          created_at = `${now.getFullYear()}-${created_at}`;
        }
        textLength = textLength || -1;
        return `("${uid}", "${itemid}", "${scheme}", "${created_at}", ${JSON.stringify(text)}, "${source}", "${textLength}")`;
      });
      if (sql.length <= 0) return;
      const { ok: dbOK, msg } = await db.execute(
        `INSERT INTO \`weibo\` (uid, item_id, scheme, create_date, text, source, text_length)
        VALUES ${sql.join(', ')};`
      );
      if (!dbOK) logError('[INSERT SQL]', msg);

      db.execute(
        `INSERT INTO \`worker_statistic\` (worker, weibo_num, timestamp)
        VALUES ('${WORKER_NAME}', '${sql.length}', '${((new Date()).getTime() / 1000) | 0}');`
      ).then(({ok, msg}) => {
        if (!ok) logError('[worker-statistic]', msg);
      }).catch(err => logError('[worker-statistic]', err));
    });

    await lock.unlock().catch(err => logError('[redlock]', err));
    await waitFor(Math.max(Math.random() * 3, 1) * 5000);
    weiboFetcher.reset();
  }
};

main().catch(err => logError(err));

/**
 * 格式化时间
 * @param {Date} time
 */
function formatTime(time) {
  const prefix = s => s.toString().padStart(2, '0');
  const year = time.getFullYear();
  const month = prefix(time.getMonth() + 1);
  const date = prefix(time.getDate());
  const hour = prefix(time.getHours());
  const minutes = prefix(time.getMinutes());
  const seconds = prefix(time.getSeconds());
  return `${year}-${month}-${date} ${hour}:${minutes}:${seconds}`;
}
